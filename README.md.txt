Submission README
=========
---


Personal Details (Students Fill In Below)
===
---
- **Student Name**: Marc Brooks
- **Student Number**: 10329586


- **Course**: Computing & Games Development
- **Module**: AINT209
- **Assignment Title**: Part 2 Term 2 Project & Documentation


1. **Blog Link**: http://www.dragonfly3r.wordpress.com
2. **Screen Capture Link**: https://www.youtube.com/watch?v=nUjq_2Skj08
3. **Credits**
 * Unity Template - Interactive Systems Studio
 * Cracked stone fill with lava - Mikelarg
 * Stone Textures Pacl - Nobiaz / Yughues
 * Stone Floor Texture Tile - 3dfancy
---


Assignment Details
===

---
###Deadline
**Thursday 16th Feb 2015 @ 23:00**

---
###Module Documentation
[AINT 209 Module Overview](https://dle.plymouth.ac.uk/pluginfile.php/206628/mod_resource/content/1/AINT209overview2014.pdf)

