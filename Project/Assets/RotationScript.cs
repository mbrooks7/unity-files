﻿using UnityEngine;
using System.Collections;

public class RotationScript : MonoBehaviour
{
	[HideInInspector]
	public float speed = 6.0f;
	[HideInInspector]
	//+ values for positive rotation & - values for negative rotation.
	public float rotation = 5.0f;

    public bool rotateX = false;
    public bool rotateY = false;
    public bool rotateZ = false;
	
    // Update is called once per frame

	void FixedUpdate()
    {
        //  Toggles X Rotation
        if (rotateX)
        {
            transform.Rotate(speed * Time.deltaTime * rotation, 0, 0);//rotates object on X axis
               
        }
        //  Toggles Y Rotation
        if (rotateY)
        {
            transform.Rotate(0, speed * Time.deltaTime * rotation, 0);//rotates object on Y axis
            
        }
        //  Toggles Z Rotation
        if (rotateZ)
        {
            transform.Rotate(0, 0, speed * Time.deltaTime * rotation);//rotates object on Z axis
            
        }

    }
 
}
