﻿using UnityEngine;
using System.Collections;

public class MovingPlatformScript : MonoBehaviour
{

    public Transform spotOfOrigin;
    public Transform spotOfDestination;
	public Transform optional2ndDestination;

	[HideInInspector]
	public int platformState = 0;
    [HideInInspector]
	public float speed = 0.05f;
	[HideInInspector]
    public bool directionChange;
	[HideInInspector]
	public bool optionalDirectionChange;

    void FixedUpdate()
    {
		float distance;
        //Cases here are used for moving platforms or elevators
		switch (platformState)
		{
			case 0:

			transform.position = Vector3.MoveTowards (transform.position, spotOfOrigin.position, speed);
			distance = Vector3.Distance (spotOfOrigin.position, transform.position);

			if (distance < 0.5)
			{
				platformState = 1;
			}
			break;

			case 1: //going towards 1
			transform.position = Vector3.MoveTowards (transform.position, spotOfDestination.position, speed);
			distance = Vector3.Distance (spotOfDestination.position, transform.position);

			if (distance < 0.5)
			{
				platformState = 0;
			}
			break;

			case 2: //going towards 2
			transform.position = Vector3.MoveTowards (transform.position, optional2ndDestination.position, speed);
			distance = Vector3.Distance (optional2ndDestination.position, transform.position);

			if (distance < 0.5)
			{
				platformState = 3;
			}
			break;

			case 3: //going towards 3
			transform.position = Vector3.MoveTowards (transform.position, spotOfDestination.position, speed);
			distance = Vector3.Distance (spotOfDestination.position, transform.position);

			if (distance < 0.5)
			{
				platformState = 0;
			}
			break;
		}
	}
    void onTriggerEnter(Collider collider)
	{

        var enter = gameObject;
        var firstPersonController = collider.GetComponent<CharacterMotor>();

        firstPersonController.transform.parent = enter.transform.parent;
    }


	
    void onTriggerExit(Collider collider)
    {
        var firstPersonController = collider.GetComponent<CharacterMotor>();
        firstPersonController.transform.parent = null;
    }

    void onCollisionEnter(Collider collider)
    {
        if (collider.tag == "blockage")
        {
            speed = 0;
        }
        else
        {
        }
    }
        
}





