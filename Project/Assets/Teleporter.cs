﻿using UnityEngine;
using System.Collections;

public class Teleporter : MonoBehaviour
{
    public Transform teleporterPoint1;
    public Transform teleporterPoint2;
	/*
	delegate bool TeleportPositionDelegate(Collider collider);

	TeleportPositionDelegate[] m_Delegates = { TeleportPoint1, TeleportPoint2 };
	
	void Update()
	{
		transform.position = m_Delegates [collider.tag](collider);
	}

	bool TeleportPoint1(Collider collider)
	{
		if (collider.tag == "Teleporter") 
		{
			transform.position = teleporterPoint1.position;
		}
	}

	bool TeleportPoint2(Collider collider)
	{
		if (collider.tag == "Teleporter2") 
		{
			transform.position = teleporterPoint2.position;
		}
	}

	*/
	/*
	delegate void MultiDelegate();
	MultiDelegate myMultiDelegate;

	void Start()
	{
		myMultiDelegate += TeleportPoint1;
		myMultiDelegate += TeleportPoint2;

		if (myMultiDelegate != null) 
		{
			myMultiDelegate();
		}
	}

	void TeleportPoint1(Collider collider)
	{
		if (collider.tag == "Teleporter") 
		{
			transform.position = teleporterPoint1.position
		}
	}

	void TeleportPoint1(Collider collider)
	{
		if (collider.tag == "Teleporter") 
		{
			transform.position = teleporterPoint1.position;
		}
	}


	void TeleportPoint2(Collider collider)
	{
		if (collider.tag == "Teleporter2") 
		{
			transform.position = teleporterPoint2.position;
		}
	}*/
    void OnTriggerEnter(Collider collider)
    {
		if (collider.tag == "Teleporter") 
		{
			transform.position = teleporterPoint1.position;
		}
		if (collider.tag == "Teleporter2") 
		{
			transform.position = teleporterPoint2.position;
		}
	}
}
