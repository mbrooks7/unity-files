﻿var isOnCountdown : boolean = false;
transform.collider.isTrigger = true;
function OnTriggerEnter(collider : Collider)
    {
        if (collider.tag == "P1Bullet")
        { 
            //If it is the bullet entering the collider
            if (!isOnCountdown)
            {
                transform.collider.isTrigger = false;
                renderer.enabled = true;
                isOnCountdown = true;
                
                Invoke("ReEnable", 10); //Re-enable it in 10 seconds
            }
           
        }
    }
 
    function ReEnable()
    {
        transform.collider.isTrigger = true;
        renderer.enabled = false;
        isOnCountdown = false;
        

       
        
    }