﻿using UnityEngine;
using System.Collections;

public class GUIInstructions : MonoBehaviour {

    void OnGUI()
    {
        GUI.Label(new Rect(5, 5, 200, 35), "Right Joystick Horizontal");
        GUI.Label(new Rect(155, 5, 200, 25), Input.GetAxis("Right Joystick Horizontal").ToString());

        GUI.Label(new Rect(5, 25, 200, 25), "Right Joystick Vertical");
        GUI.Label(new Rect(155, 25, 200, 25), Input.GetAxis("Right Joystick Vertical").ToString());

        GUI.Label(new Rect(5, 45, 200, 25), "Left Joystick Vertical");
        GUI.Label(new Rect(155, 45, 200, 25), Input.GetAxis("Left Joystick Vertical").ToString());

        GUI.Label(new Rect(5, 65, 200, 25), "Left Joystick Horizontal");
        GUI.Label(new Rect(155, 65, 200, 25), Input.GetAxis("Left Joystick Horizontal").ToString());

        GUI.Label(new Rect(5, 85, 200, 25), "D-Pad Vertical");
        GUI.Label(new Rect(155, 85, 200, 25), Input.GetAxis("D-Pad Vertical").ToString());

        GUI.Label(new Rect(5, 105, 200, 25), "D-Pad Horizontal");
        GUI.Label(new Rect(155, 105, 200, 25), Input.GetAxis("D-Pad Horizontal").ToString());

        GUI.Label(new Rect(5, 125, 200, 25), "Back Button");
        GUI.Label(new Rect(155, 125, 200, 25), Input.GetButton("Back Button").ToString());

        GUI.Label(new Rect(5, 145, 200, 25), "Start Button");
        GUI.Label(new Rect(155, 145, 200, 25), Input.GetButton("Start Button").ToString());

        GUI.Label(new Rect(5, 165, 200, 25), "A-Button");
        GUI.Label(new Rect(155, 165, 200, 25), Input.GetButton("A-Button").ToString());

        GUI.Label(new Rect(5, 185, 200, 25), "B-Button");
        GUI.Label(new Rect(155, 185, 200, 25), Input.GetButton("B-Button").ToString());

        GUI.Label(new Rect(5, 205, 200, 25), "Y-Button");
        GUI.Label(new Rect(155, 205, 200, 25), Input.GetButton("Y-Button").ToString());

        GUI.Label(new Rect(5, 225, 200, 25), "X-Button");
        GUI.Label(new Rect(155, 225, 200, 25), Input.GetButton("X-Button").ToString());

        GUI.Label(new Rect(5, 245, 200, 25), "RB");
        GUI.Label(new Rect(155, 245, 200, 25), Input.GetButton("RB").ToString());

        GUI.Label(new Rect(5, 265, 200, 25), "LB");
        GUI.Label(new Rect(155, 265, 200, 25), Input.GetButton("LB").ToString());

        GUI.Label(new Rect(5, 285, 200, 25), "Left Trigger");
        GUI.Label(new Rect(155, 285, 200, 25), Input.GetAxis("Left Trigger").ToString());

        GUI.Label(new Rect(5, 305, 200, 25), "Right Trigger");
        GUI.Label(new Rect(155, 305, 200, 25), Input.GetAxis("Right Trigger").ToString());
    }
}
