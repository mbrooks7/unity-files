﻿
var crosshairTex : Texture2D; // cross hair image goes here
var position : Rect; // position of the crosshair image
static var OriginalOn = true;

function Update () 
{
    
    position = Rect(Screen.width/4 - crosshairTex.width/2, (Screen.height - crosshairTex.height) / 2, crosshairTex.width, crosshairTex.height); //Determines width/height of our cross hair

}

function OnGUI () 
{
    if(OriginalOn == true)
    {
        GUI.DrawTexture(position, crosshairTex); //Draws crosshair texture
        Screen.showCursor = false; //Disables cursor from being visible
    }
}