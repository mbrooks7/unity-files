﻿Submission README
=========

---




Personal Details (Students Fill In Below)

===

---

- **Student Name**: <<Marc Brooks>>

- **Student Number**: <<10329586>>




- **Course**: Computing & Games Development

- **Module**: AINT209

- **Assignment Title**: <<Balcaria Rift (Version 1.0)>>




1. **Blog Link**: <<https://www.dragonfly3r.wordpress.com>>

2. **Screen Capture Link**: <<https://www.dragonfly3r.wordpress.com>>

3. **Credits**
 * Unity Template - Interactive Systems Studio
 
* <<Matthew Plummer for help fixing game code>>

---




Assignment Details
===


---

###Deadline

**Thursday 6th Nov 2014 @ 23:00**



---
###Module Documentation

[AINT 209 Module Overview](https://dle.plymouth.ac.uk/pluginfile.php/206628/mod_resource/content/1/AINT209overview2014.pdf)



---

###Submission Requirements

- You have added **iss-plymouth** as a user to your repository (see [instructions here](http://homepage.iss.io/bitbucket-add-user.html))

- There is a folder called **Submission** in the root of your repo (all of you submission files will live in here)

- Provide the following files:

    1. A pdf of your **One Page Description** 

    2. A compiled **Windows Binary (.exe)** of your *Unity Project*

    3. A *youtube* or *vimeo* link to a **Screen Capture** of your project

    4. A link to your development blog

    5. A pdf **summary feedback of your project**



--- 

###Submission Folder Layout



```

/

├── README.md

├── /Submission

    ├── Project Description.pdf

    ├── /Windows Binary

        ├── Submission.exe

        └── /Submission Data

    ├── YouTube/Vimeo Screen Capture Link

    ├── Tumblr/Development blog link

    └── Summary Feedback Evaluation.pdf





```



---

###Assignment Briefing

Duality Prototype; A 2 player, single screen cooperative game using alternate controllers.



*Learning Outcome*: Identify and implement appropriate solutions for individual project.



**Part 1** (6 week introductory project term 1)



Duality - A co-op single screen multiplayer game developed in Unity 3D

Demo with student/staff peer review in session 6

Iterative development blog (to include minimum of weekly post/captures/embedded
 
demo/solution)



GIT repository with readme evaluation (feedback from peer/staff review, Play

through screen capture and desktop build (latest iteration) refer to GIT template for

deliverables.



**25%** Objective is to explore relevant game mechanics for different forms of co

operative game play on a single screen, for example player one could use a leap

motion to walk across a tight rope over a ravine while player two blows into a

microphone to clear fog, aiding player one to proceed, revealing obstacles and

avoiding danger etc



Provide an initial one page outline of initial concept identifying core mechanics

and controllers used. (Add to pivotal tracker story or developer blog)



Plan a series experiments to test and iterate your core mechanics (Add 

documents to pivotal tracker stories and/or developer blog).



Implement experiments in Unity and test with peer group (document with 

screen/movie captures)



Integrate most successful experiments into a single coop game for two players 

on a single screen using two different controllers.



Provide alternate control set up for two players on a standard keyboard (as a 

backup for when external controllers un available – ie: WASD, cursor keys, 

control, option, space and escape.